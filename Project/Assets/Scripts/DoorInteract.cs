﻿using UnityEngine;
using System.Collections;

public class DoorInteract : MonoBehaviour
{
    // Private variables
    // Accessible in the editor
    [SerializeField]
    private Transform theDoor;

    // Code optimisation
    private Animator theAnimator;

    // Use this for initialization
    void Start()
    {
        theAnimator = theDoor.GetComponent<Animator>();
    }

    private void ActivateDoor()
    {
        theAnimator.SetBool("Animating", true);
        Invoke("DeactivateDoor", 0.1f);
    }

    private void DeactivateDoor()
    {
        theAnimator.SetBool("Animating", false);
    }

    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && Input.GetKeyDown(KeyCode.E))
        {
            ActivateDoor();
        }
    }
}