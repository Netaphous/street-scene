﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TypeText : MonoBehaviour
{
    // Private variables
    // Accessible in the editor
    [SerializeField]
    private float typeDelay = 0.1f;
    [SerializeField]
    private Text textBox;
    [SerializeField]
    private GameObject displayPanel;

    // Script logic
    private string currentString = "";
    private string targetString = "";
    private int counter = 0;
    private bool running = true;

    void Start()
    {
        StartCoroutine("TypePerLetter");
    }

    public void HidePopup()
    {
        displayPanel.SetActive(false);
    }

    public void StartTypePerLetter(string textToType)
    {
        currentString = "";
        targetString = textToType;
        counter = 0;

        displayPanel.SetActive(true);
    }

    public void DisplayFullMessage(string textToType)
    {
        currentString = textToType;
        counter = -1;

        displayPanel.SetActive(true);
    }

    IEnumerator TypePerLetter()
    {
        while (running)
        {
            char currentChar = '#';
            if (counter >= 0 && counter < targetString.Length)
            {
                currentChar = targetString[counter];

                currentString += currentChar;

                counter++;
            }

            textBox.text = currentString;

            float realDelay = typeDelay;

            if(currentChar == '#')
            {
                realDelay = 0.01f;
            }
            else if (currentChar == '.' || currentChar == '?' || currentChar == '!')
            {
                realDelay = typeDelay * 5;
            }

            yield return new WaitForSeconds(realDelay);
        }
    }
}
