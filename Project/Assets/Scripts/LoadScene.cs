﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace Netaphous.Utilities
{
    public class LoadScene : MonoBehaviour
    {
        public void LoadLevel(int level)
        {
            SceneManager.LoadScene(level);
        }

        public void LoadLevel(string name)
        {
            SceneManager.LoadScene(name);
        }

        public void ExitGame()
        {
            Application.Quit();
        }
    }
}