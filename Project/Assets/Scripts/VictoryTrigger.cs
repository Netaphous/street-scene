﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class VictoryTrigger : MonoBehaviour
{
    // Private variables
    // Accessible in the editor
    [SerializeField]
    private GameObject victoryPanel;

    void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            victoryPanel.SetActive(true);
            GameObject.FindWithTag("Player").GetComponent<FirstPersonController>().enabled = false;
            Cursor.lockState = CursorLockMode.None;
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = true;
        }
    }
}