﻿using UnityEngine;
using System.Collections;

public class SpeechHandler : MonoBehaviour
{
    [SerializeField]
    private AudioClip KillSpeech;

    private bool played = false;

    // Update is called once per frame
    public void PlayKill()
    {
        if (!played)
        {
            played = true;
            GetComponent<AudioSource>().Stop();
            GetComponent<AudioSource>().clip = KillSpeech;
            Invoke("RealPlay", 1.0f);
        }
    }

    private void RealPlay()
    {
        GetComponent<AudioSource>().Play();

    }
}
