﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TargetManager : MonoBehaviour
{
    // Private variables
    // Static variables
    private static string targetIdentifier = "None";
    private static bool isTargetKilled = false;

    // Accessible in the editor
    [SerializeField]
    private SpeechHandler killSpeech;

    void Start()
    {
        isTargetKilled = false;
    }

    // Use this for initialization
    void OnEnable ()
    {
        GameObject[] NPCs = GameObject.FindGameObjectsWithTag("NPC");

        List<AIController> controllers = new List<AIController>();
        AIController tempController;

        for (int i = 0; i < NPCs.Length; i++)
        {
            tempController = NPCs[i].GetComponent<AIController>();
            if(tempController.identifier != "Generic")
            {
                controllers.Add(tempController);
            }
        }

        int randomIndex = Random.Range(0, controllers.Count - 1);
        
        targetIdentifier = controllers[randomIndex].identifier;
	}

    void Update()
    {
        if(isTargetKilled)
        {
            killSpeech.PlayKill();
        }
    }

    public static void TestTargetKilled(string identifier)
    {
        if(identifier == targetIdentifier)
        {
            isTargetKilled = true;
            print("Target Killed!");
        }
    }

    public static bool GetIsTargetKilled()
    {
        return isTargetKilled;
    }

    public static string GetTargetIdentifier()
    {
        return targetIdentifier;
    }
}
