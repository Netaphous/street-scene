﻿using UnityEngine;
using System.Collections;

public class DaggerController : MonoBehaviour
{
    // Private variables
    // Accessible in the editor
    [SerializeField]
    private float stabSpeed = 0.1f;
    [SerializeField]
    private float stabDistance = 1.0f;

    // Script logic
    private bool isStabbing = false;
    private bool stabForward = true;
    private Vector3 startPos;
    private bool playing = false;

    // Code optimisation
    private Transform theTransform;

    // Use this for initialization
    void Start()
    {
        theTransform = transform;

        startPos = theTransform.localPosition;
    }

    // Update is called once per frame
    /// <summary>
    /// 
    /// </summary>
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !isStabbing)
        {
            isStabbing = true;
        }

        if (isStabbing)
        {
            Vector3 nextPos = theTransform.localPosition;

            if (stabForward)
            {
                if (theTransform.localPosition.z > startPos.z + stabDistance)
                {
                    stabForward = false;
                }
                else
                {
                    nextPos.z += stabSpeed;
                }
            }
            else
            {
                if (theTransform.localPosition.z < startPos.z)
                {
                    stabForward = true;
                    isStabbing = false;
                }
                else
                {
                    nextPos.z -= stabSpeed;
                }
            }
            theTransform.localPosition = nextPos;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        AIController AI = other.GetComponent<AIController>();

        if (isStabbing && AI != null)
        {
            print(AI.identifier);

            if (!playing)
            {
                GetComponent<AudioSource>().Play();
                playing = true;
                Invoke("StopPlaying", GetComponent<AudioSource>().clip.length);
            }

            other.gameObject.SendMessage("Die");

            other.GetComponent<Rigidbody>().velocity = transform.forward * 2;
            TargetManager.TestTargetKilled(AI.identifier);
        }
    }

    private void StopPlaying()
    {
        playing = false;
    }

    public bool CanUnequip()
    {
        return !isStabbing;
    }
}