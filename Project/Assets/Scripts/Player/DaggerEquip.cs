﻿using UnityEngine;
using System.Collections;

public class DaggerEquip : MonoBehaviour
{
    // Public variables
    // To be accessed by other scripts
    public static bool daggerUnsheathed = false;
    public AudioClip unsheathSound;
    public AudioClip sheathSound;

    // Private variables
    // Script logic
    private bool animating = false;

    // Code optimisation
    private DaggerController dagger;
    private AudioSource theAudioSource;

    void Start()
    {
        dagger = transform.GetChild(0).GetComponent<DaggerController>();
        theAudioSource = GetComponent<AudioSource>();

        daggerUnsheathed = false;
    }

    void Update()
    {
        if (!animating &&
            dagger.CanUnequip() &&
            Input.GetKeyDown(KeyCode.Q))
        {
            if(!daggerUnsheathed)
            {
                EquipDagger();
            }
            else
            {
                UnequipDagger();
            }
        }
    }

    private void EquipDagger()
    {
        daggerUnsheathed = true;
        theAudioSource.clip = unsheathSound;
        theAudioSource.Play();
        animating = true;
        Invoke("SetDagger", theAudioSource.clip.length);
    }

    private void UnequipDagger()
    {
        daggerUnsheathed = false;
        theAudioSource.clip = sheathSound;
        theAudioSource.Play();
        animating = true;
        Invoke("SetDagger", theAudioSource.clip.length);
    }

    private void SetDagger()
    {
        dagger.gameObject.SetActive(daggerUnsheathed);
        animating = false;
    }
}
