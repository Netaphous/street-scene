﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class VisionCone : MonoBehaviour
{
    [SerializeField]
    private float gameOverDelay = 3.0f;
    [SerializeField]
    private LayerMask mask;
    private bool alert = false;

    private DaggerEquip dagger;

    private GameObject warningPanel;
    private GameObject lossPanel;
    private FirstPersonController player;
    private AIController AI;

    void Start()
    {
        warningPanel = GameObject.Find("GameOver").transform.GetChild(1).gameObject;
        lossPanel = GameObject.Find("GameOver").transform.GetChild(0).gameObject;
        player = GameObject.FindWithTag("Player").GetComponent<FirstPersonController>();
        AI = transform.parent.parent.GetComponent<AIController>();
    }

    void OnTriggerStay(Collider other)
    {
        dagger = other.GetComponentInChildren<DaggerEquip>();

        if (dagger != null)
        {
            RaycastHit theHit;
            Physics.Raycast(transform.parent.position, other.transform.position - transform.parent.position, out theHit);
            if (DaggerEquip.daggerUnsheathed &&
                !alert &&
                theHit.collider.gameObject.name == "Player")
            {
                MakeAlert();
            }
        }
    }

    private void MakeAlert()
    {
        Invoke("GameOver", gameOverDelay);
        warningPanel.SetActive(true);
    }

    void Update()
    {
        if (AI.isDead)
        {
            CancelInvoke();
            warningPanel.SetActive(false);
            Destroy(gameObject);
        }
    }

    private void GameOver()
    {
        lossPanel.SetActive(true);
        warningPanel.SetActive(false);
        player.enabled = false;
        Cursor.lockState = CursorLockMode.None;
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;
    }
}
