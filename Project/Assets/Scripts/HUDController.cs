﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HUDController : MonoBehaviour
{
    // Private variables
    // Accessible in the editor
    [SerializeField]
    private Text targetText;

	// Use this for initialization
	void Start ()
    {
        targetText.text = "Target: " + TargetManager.GetTargetIdentifier();
	}
	
	// Update is called once per frame
	void Update ()
    {
	    if(TargetManager.GetIsTargetKilled())
        {
            targetText.text = "Target killed!";
        }
	}
}
