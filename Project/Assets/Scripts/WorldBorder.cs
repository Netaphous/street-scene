﻿using UnityEngine;
using System.Collections;

public class WorldBorder : MonoBehaviour
{
    // Private variables
    // Accessible in the editor
    [SerializeField]
    private GameObject exitWarning;

    // Script logic
    private GameObject borderCollider;

    void Start()
    {
        borderCollider = transform.GetChild(0).gameObject;

        borderCollider.SetActive(true);
    }

    void Update()
    {
        if(TargetManager.GetIsTargetKilled())
        {
            borderCollider.SetActive(false);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player" && !TargetManager.GetIsTargetKilled())
        {
            exitWarning.SetActive(true);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            exitWarning.SetActive(false);
        }
    }
}
