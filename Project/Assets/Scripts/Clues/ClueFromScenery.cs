﻿using UnityEngine;

public class ClueFromScenery : GiveClue
{
    // Private variables
    // Accessible in the editor
    [SerializeField]
    private float displayDelay = 1.0f;

    // Code optimisation
    private TypeText typeText;
    private Animator theAnimator;    

	// Use this for initialization
	void Start ()
    {
        theAnimator = GetComponent<Animator>();

        typeText = GameObject.Find("PopupText").GetComponent<TypeText>();
    }

    protected override float Activate()
    {
        if (theAnimator != null)
        {
            theAnimator.SetBool("Opening", true);
            theAnimator.SetBool("Closing", false);
        }
        Invoke("DisplayMessage", displayDelay);
        return interactionDelay;
    }

    private void DisplayMessage()
    {
        counter++;
        if(counter >= clueText.Length)
        {
            counter = 0;
        }

        typeText.DisplayFullMessage(clueText[counter]);
    }

    protected override void Deactivate()
    {
        if (theAnimator != null)
        {
            theAnimator.SetBool("Opening", false);
            theAnimator.SetBool("Closing", true);
        }
        typeText.HidePopup();
    }
}
