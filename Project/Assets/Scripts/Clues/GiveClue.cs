﻿using UnityEngine;

public abstract class GiveClue : MonoBehaviour
{
    // Private variables
    // Accessible in the editor
    [SerializeField]
    protected string[] clueText;
    [SerializeField]
    protected float interactionDelay = 1.0f;

    protected int counter = 0;

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player" &&
            Input.GetKeyDown(KeyCode.E))
        {
            ShowClue();
        }
    }

    private void ShowClue()
    {
        CancelInvoke();

        float clueDelay = Activate();

        Invoke("HideClue", clueDelay);
    }

    public void HideClue()
    {
        Deactivate();
    }

    protected abstract float Activate();

    protected abstract void Deactivate();
}