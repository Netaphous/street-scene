﻿using UnityEngine;

public class ClueFromCharacter : GiveClue
{
    // Private variables
    // Code optimisation
    private TypeText typeText;
    private AIController AI;

    // Use this for initialization
    void Start ()
    {
        AI = transform.parent.GetComponent<AIController>();  

        typeText = GameObject.Find("PopupText").GetComponent<TypeText>();
	}

    protected override float Activate()
    {
        if (!AI.isDead)
        {
            counter++;
            if (counter >= clueText.Length)
            {
                counter = 0;
            }

            typeText.StartTypePerLetter(clueText[counter]);
            return interactionDelay;
        }
        return -1;
    }

    protected override void Deactivate()
    {
        typeText.HidePopup();
    }
}
