﻿using UnityEngine;
using System.Collections;

public class AIController : MonoBehaviour
{
    // Public variables
    // To be set in the editor
    public string identifier = "None";
    [HideInInspector]
    public bool isDead = false;

    // Private variables
    // Code optimisation
    private Rigidbody theRigidbody;
    private AudioSource theAudioSource;

    void Start()
    {
        theRigidbody = GetComponent<Rigidbody>();
        theAudioSource = GetComponent<AudioSource>();
    }

    private void Die()
    {
        if (!isDead)
        {
            theRigidbody.isKinematic = false;
            theAudioSource.Play();
            isDead = true;
        }
    }
}
